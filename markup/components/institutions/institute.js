$('.aside-contact__header').on('click', function () {
   $(this).parent().toggleClass('show');
});

var gallerySlider = new Swiper('.gallery-swiper', {
    speed: 400,
    slidesPerView: 3,
    spaceBetween: 30,
    loop: true,
    navigation: {
        nextEl: '.gallery-slider-next',
        prevEl: '.gallery-slider-prev',
    },
    breakpoints: {
        576: {
            slidesPerView: 2,
            spaceBetween: 15,
        },
        992: {
            slidesPerView: 3,
        },
    }
});
