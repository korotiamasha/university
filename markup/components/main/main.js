var mainBanner = new Swiper('.main-banner__slider', {
    speed: 400,
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    pagination: {
        el: '.main-banner__pagination',
        clickable: true,
        type: 'bullets',
    },
});

var mainInfo = new Swiper('.main-info__slider', {
    speed: 400,
    slidesPerView: 1,
    spaceBetween: 30,
    slidesPerColumn: 2,
    slidesPerGroup: 2,
    loop: false,
    navigation: {
        nextEl: '.main-info__next',
        prevEl: '.main-info__prev',
    },
    pagination: {
        el: '.main-info__pagination',
        clickable: true,
        type: 'bullets',
    },
    breakpoints: {
        576: {
            slidesPerView: 2,
            spaceBetween: 15,
            loop: true,
            slidesPerColumn: 1,
            slidesPerGroup: 2,
        },
        992: {
            slidesPerView: 3,
            loop: true,
            slidesPerColumn: 1,
            slidesPerGroup: 3,
        },
    }
});


var mainCat = new Swiper('.main-cat-slider', {
    speed: 400,
    slidesPerView: 'auto',
    spaceBetween: 0,
    loop: true,
    slidesPerGroup: 1,

    navigation: {
        nextEl: '.main-cat-next',
        prevEl: '.main-cat-prev',
    },
    breakpoints: {
        576: {
            spaceBetween: 20,
        },
        992: {
            spaceBetween: 30,
        },
    }
});

var mainPartners = new Swiper('.main-partners__slider', {
    speed: 400,
    slidesPerView: 2,
    spaceBetween: 10,
    loop: true,

    navigation: {
        nextEl: '.main-partners-next',
        prevEl: '.main-partners-prev',
    },
    breakpoints: {
        576: {
            slidesPerView: 4,
        },
        992: {
            slidesPerView: 6,
            spaceBetween: 30,
        },
    }
});

var dates = [
    new Date('2020-10-15'),
    new Date('2020-10-16'),
    new Date('2020-10-17'),
    new Date('2020-11-17'),
    new Date('2020-11-18'),
    new Date('2020-11-19'),
    new Date('2020-11-23'),
    new Date('2020-11-24'),
    new Date('2020-11-25'),
    new Date('2020-11-26'),
    new Date('2020-11-30'),
];
$(".datepicker-here").datepicker({

});
